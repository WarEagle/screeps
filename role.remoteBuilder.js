var roleBuilder = require('role.builder');
var helperNavigation = require('helper.navigation');
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.remoteBuilder');
 * mod.thing == 'a thing'; // true
 */

let thereAreConstructionSites = function (creep) {
// are there construction sites?
    let constructionSites = creep.room.find(FIND_CONSTRUCTION_SITES);
    if (constructionSites == undefined || constructionSites.length == 0) {
        //no? then rest at flag
        //TODO what about repairs?
        //console.log(JSON.stringify(constructionSites));

        for (let flagName in Game.flags) {
            let flag = Game.flags[flagName];
            // console.log(JSON.stringify(flag));
            // console.log(JSON.stringify(flag.room));
            // console.log(JSON.stringify(creep.room));
            // if (flag != undefined && flag.pos.roomName == creep.room.name) {
            //     creep.moveTo(flag.pos);
            //     break;
            // }
        }
        return false;
    } else {
        return true;
    }
};
module.exports = {
    run: function (creep) {


        // correct room?
        if (helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            roleBuilder.run(creep);
        }
    }

};