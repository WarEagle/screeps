/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.harvester');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function (creep) {
        let x = creep.memory.x;
        let y = creep.memory.y;
        let sourceId = creep.memory.sourceId;

        if (creep.pos.x != x || creep.pos.y != y) {
            // let path = creep.pos.findPathTo(x, y);
            // creep.moveByPath(path);
            creep.moveTo(x,y);
        }else{
            //collect energy
            let source = Game.getObjectById(sourceId);
            creep.harvest(source);
        }
    }
}
;