let roleUpgrader = require('role.upgrader');

/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * let  mod = require('role.recharger');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function (creep) {

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working == false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working == true) {
            let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_TOWER
                && s.energyCapacity * 0.9 > s.energy
                && s.room == creep.room
            });
            if (structure != undefined) {
                if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                }
            } else {
                let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (s) => s.structureType == STRUCTURE_LAB
                    && s.energyCapacity * 0.9 > s.energy
                    && s.room == creep.room
                });
                if (structure != undefined) {
                    if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(structure);
                    }
                } else {
                    //perform upgrade
                    roleUpgrader.run(creep);
                }
            }
        } else {
            //take from storage
            let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_STORAGE
                && s.room == creep.room
            });
            if (structure != undefined) {
                if (structure.transfer(creep, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                }
            } else {
                //harvest
                let source = creep.pos.findClosestByPath(FIND_SOURCES, {
                        filter: (e) => {
                            return e.energy > 0;
                        }
                    }
                );
                if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(source);
                }
            }
        }
    }
};