/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.upgrader');
 * mod.thing == 'a thing'; // true
 */
let helperNavigation = require('helper.navigation');

module.exports = {
    run: function (creep) {

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working == false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working == true) {
            if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
        } else {
            if (helperNavigation.canTakeEnergyFromReceiverLink(creep)) {
            } else {
                //take from storage
                let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (s) => s.structureType == STRUCTURE_STORAGE
                    && s.room == creep.room
                });
                if (structure != undefined) {
                    if (structure.transfer(creep, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(structure);
                    }
                } else {
                    let source = creep.pos.findClosestByPath(FIND_SOURCES, {
                            filter: (e) => {
                                return e.energy > 0
                                    && e.room == creep.room;

                            }
                        }
                    );
                    if (source != undefined) {
                        if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(source);
                        }
                    } else {
                        //TODO don't use fixed string!
                        // let storage = Game.getObjectById('5845dee4c02371a641977d27');
                        // if (creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        //     creep.moveTo(storage);
                        // }

                    }
                }
            }
        }
    }
};