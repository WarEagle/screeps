/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * let  mod = require('role.recharger');
 * mod.thing == 'a thing'; // true
 */
let helperNavigation = require('helper.navigation');

module.exports = {
    run: function (creep) {

        if (creep.memory.working == true) {
            let hasLoad = false;
            for (let mineral in RESOURCES_ALL) {
                if (creep.carry[RESOURCES_ALL[mineral]] > 0) {
                    hasLoad = true;
                    break;
                }
            }
            creep.memory.working = hasLoad;
        }

        if (creep.memory.working == false) {
            let carryTotal = 0;
            for (let mineral in RESOURCES_ALL) {
                if (creep.carry[RESOURCES_ALL[mineral]]) {
                    carryTotal += creep.carry[RESOURCES_ALL[mineral]];
                }
            }
            // console.log(JSON.stringify(creep.carry));
            // console.log(carryTotal);
            if (carryTotal >= creep.carryCapacity * 0.9) {
                creep.memory.working = true;
            }
        }


        if (creep.ticksToLive < 200) {
            creep.memory.working = true;
        }

        if (creep.memory.working == true) {
            //unload

            //correct room?
            if (creep.pos.roomName != creep.memory.baseRoomName) {
                let route = Game.map.findRoute(creep.pos.roomName, creep.memory.baseRoomName);
                let exit = Game.map.findExit(creep.room, route[0].room);
                let direction = creep.pos.findClosestByRange(exit);
                //console.log(JSON.stringify(direction));
                creep.moveTo(direction);
            } else {
                if (helperNavigation.unloadingEnergyToStorage(creep)) {
                } else if (helperNavigation.unloadingMineralsToStorage(creep)) {
                }
            }

        }
        else {
            //load

            //correct room?
            if (creep.pos.roomName != creep.memory.targetRoomName) {
                let route = Game.map.findRoute(creep.pos.roomName, creep.memory.targetRoomName);
                //console.log(JSON.stringify(creep.pos.roomName));
                // console.log(JSON.stringify(creep.memory.targetRoomName));
                // console.log(JSON.stringify(route));
                // console.log(JSON.stringify(creep));
                let exit = Game.map.findExit(creep.room, route[0].room);
                let direction = creep.pos.findClosestByRange(exit);
                creep.moveTo(direction);
            } else {
                if (helperNavigation.takeMineralsFromStorage(creep)) {
                } else if (helperNavigation.takeEnergyFromStorage(creep)) {
                } else if (helperNavigation.takeEnergyFromEnemyBuilding(creep)) {
                }
            }
        }
    }
}
;
