/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('maintenance.links');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    transfer: function (roomName) {
        if (Game.time % 3 != 0){
            //let's save some cpu
            return;
        }

        let configuredEnergyLinks = roomConstants[roomName].energyLink;
        for (let link in configuredEnergyLinks) {
            //console.log(JSON.stringify(configuredEnergyLinks[link]));
            let loadingLink = Game.getObjectById(configuredEnergyLinks[link].sourceLink);
            let unloadingLink = Game.getObjectById(configuredEnergyLinks[link].targetLink);

            if (unloadingLink.energyCapacity *0.9 > unloadingLink.energy
                && loadingLink.energy > 0) {
                loadingLink.transferEnergy(unloadingLink);
            }
        }
    }
};