/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.harvester');
 * mod.thing == 'a thing'; // true
 */
let helperNavigation = require('helper.navigation');

module.exports = {
    run: function (creep) {

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working == false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working == true) {
            //distribute energy
            if (helperNavigation.unloadingToSpawnOrExtension(creep)) {
            } else {
                helperNavigation.unloadingEnergyToStorage(creep);
            }
        } else {
            //collect energy
            let source = creep.pos.findClosestByPath(FIND_SOURCES, {
                    filter: (x) => (x.energy > 0
                    && x.room == creep.room)

                })
                ;
            if (source) {
                if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(source);
                }
            } else {
                if (helperNavigation.canTakeEnergyFromReceiverLink(creep)) {
                }
            }
        }
    }
}
;