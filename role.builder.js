var roleUpgrader = require('role.upgrader');
var roleRepairer = require('role.repairer');

/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.builder');
 * mod.thing == 'a thing'; // true
 */

function dismantlingEnemyBuilding(creep) {
    //TODO needs optimization
    return false;

    var building = creep.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES, {
        filter: (x) => (
            x.room == creep.room
            && x.structureType != STRUCTURE_ROAD
            && x.structureType != STRUCTURE_RAMPART
            && x.structureType != STRUCTURE_WALL
            && x.structureType != STRUCTURE_STORAGE
            && x.structureType != STRUCTURE_LAB
        )
    });
    if (building){
        //console.log(building);
        let tmp = creep.dismantle(building);
        if (tmp == ERR_NOT_IN_RANGE){
            creep.moveTo(building);
        }
        //console.log(tmp);
        return true;
    }
    return false;
}

function collectEnergyFromGround(creep) {
    var droppedEnergy = creep.pos.findClosestByPath(FIND_DROPPED_ENERGY, {
        filter: (x) => (
            x.room == creep.room
            && x.energy >= 50
        )
    });
    if (droppedEnergy){
        //console.log(JSON.stringify(droppedEnergy));
        let tmp = creep.pickup(droppedEnergy);
        if (tmp == ERR_NOT_IN_RANGE){
            creep.moveTo(droppedEnergy);
        }
        //console.log(tmp);
        return true;
    }
    return false;
}

module.exports = {
    run: function (creep) {

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working == false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working != false && creep.memory.working != true) {
            creep.memory.working = false;
        }

        if (creep.memory.working == true) {
            var constructionSite = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES, {
                filter: (x) => (
                    x.room == creep.room
                )
            });
            if (constructionSite != undefined) {
                if (creep.build(constructionSite) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(constructionSite);
                }
            } else {
                roleRepairer.run(creep);
            }
        } else {
            //get energy
            if (collectEnergyFromGround(creep)){

            }else if (dismantlingEnemyBuilding(creep)){

            }else{
                //take from storage
                let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (s) => s.structureType == STRUCTURE_STORAGE
                    && s.store[RESOURCE_ENERGY] >= creep.carryCapacity
                    && s.room == creep.room
                });
                if (structure != undefined) {
                    if (structure.transfer(creep, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(structure);
                    }
                } else {
                    //take from container
                    let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                        filter: (s) => s.structureType == STRUCTURE_CONTAINER
                        && s.store[RESOURCE_ENERGY] >= creep.carryCapacity
                        && s.room == creep.room
                    });
                    if (structure != undefined) {
                        //console.log(1);
                        if (structure.transfer(creep, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(structure);
                        }
                    } else {
                        // console.log(creep.name);
                        var source = creep.pos.findClosestByPath(FIND_SOURCES, {
                                filter: (x) => (
                                    x.energy > 0
                                    && x.room == creep.room
                                )
    
                            })
                            ;
                        if (source != undefined) {
                            if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                                creep.moveTo(source);
                            }
                        } else {
                            //TODO
                            // let storage = Game.getObjectById('5845dee4c02371a641977d27');
                            // if (creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                            //     creep.moveTo(storage);
                            // }
                        }
                    }
                }
            }
        }
    }
};