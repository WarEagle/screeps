var links = require('maintenance.links');

//var buildingTower = require('building.Tower');
var spawner = require('spawner');
var tower = require('defence.tower');

/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('room.e39n55');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    manage: function (roomName, spawn) {
        spawner.spawn(roomName, spawn);
        links.transfer(roomName);

        if (!tower.defend(roomName)) {
            for (var name in Game.structures) {
                var structure = Game.structures[name];
                if (structure.structureType == STRUCTURE_TOWER) {
                    //console.log(Game.structures[name].pos);
                    tower.repair(structure);
                }
            }
        }
    }
};