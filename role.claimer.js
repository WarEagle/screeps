let helperNavigation = require('helper.navigation');
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * let mod = require('role.claimer');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function (creep) {

        // correct room?
        if (helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {
            if (creep.memory.claim == true) {
                let result = creep.claimController(creep.room.controller);
                //console.log(result);
                if (result == ERR_NOT_IN_RANGE) {
                    let rc = creep.moveTo(creep.room.controller.pos);
                } else if (result == ERR_GCL_NOT_ENOUGH) {
                    //console.log("ERROR Your Global Control Level is not enough. " + creep.memory.targetRoomName);
                    let rc2 = creep.reserveController(creep.room.controller);
                    if (rc2 == ERR_NOT_IN_RANGE) {
                        let rc = creep.moveTo(creep.room.controller.pos);
                    }
                }
            } else {
                //just reserve
                let rc2 = creep.reserveController(creep.room.controller);
                if (rc2 == ERR_NOT_IN_RANGE) {
                    let rc = creep.moveTo(creep.room.controller.pos);
                }
            }
        }
    }
};