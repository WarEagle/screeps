/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('helper.constants');
 * mod.thing == 'a thing'; // true
 */

roomConstants = [];
roomConstants['E39N55'] = {
    displayName: 'MainBase   ',

    targetHarvester: 0,
    targetUpgrader: 2,
    targetBuilder: 1,
    targetRepairer: 0,
    targetRecharger: 0,
    targetMiner: 1,
    targetLorry: 2,
    energySourceLink: '5845ecf0e519521e75c122ae',
    remoteBuilder: [
        {targetRoomName: 'E37N54', cnt: 1}, //support for new base
        {targetRoomName: 'E39N54', cnt: 1},
        {targetRoomName: 'E38N55', cnt: 1},
    ],
    remoteHarvester: [
        {targetRoomName: 'E39N54', unloadingLink: '58451de614600a462df717ae', cnt: 3},
        {targetRoomName: 'E38N55', unloadingLink: undefined, cnt: 2},
    ],
    remoteClaimer: [
        {targetRoomName: 'E39N54'},
    ],
    defender: [
        {targetRoomName: 'E39N54', cnt: 1},
        {targetRoomName: 'E38N55', cnt: 1},
    ],
    energyLink: [
        {sourceLink: '584fce9ea8eedc5f46dd2b7f', targetLink: '5845ecf0e519521e75c122ae'},
        {sourceLink: '58451de614600a462df717ae', targetLink: '584fce9ea8eedc5f46dd2b7f'},
    ],
    fixedHarvesters: [
        {x: 24, y: 13, sourceId: '579fa9d20700be0674d2ff2e'},
        {x: 16, y: 21, sourceId: '579fa9d20700be0674d2ff2f'},
    ],
    lorryRoomtransfer: [
        {targetRoomName: 'E38N53', cnt: 1},
    ]
};

roomConstants['E38N53'] = {
    displayName: 'Expansion 1',

    targetHarvester: 0,
    targetUpgrader: 1,
    targetBuilder: 1,
    targetRepairer: 0,
    targetRecharger: 0,
    targetMiner: 0,
    targetLorry: 2,
    energySourceLink: '58506a421ca50dfa68a96d68',
    remoteBuilder: [
        {targetRoomName: 'E37N54', cnt: 1}, //support for new base
        {targetRoomName: 'E38N52', cnt: 1},
        // {targetRoomName: 'E37N53', cnt: 1},
        {targetRoomName: 'E39N53', cnt: 1},
    ],
    remoteHarvester: [
        {targetRoomName: 'E38N52', unloadingLink: '5850aaca73fff7526c371aab', cnt: 3},
        {targetRoomName: 'E37N53', unloadingLink: undefined, cnt: 2},
        {targetRoomName: 'E39N53', unloadingLink: '5857394b75adc0fb4d3df691', cnt: 2},
        {targetRoomName: 'E38N54', unloadingLink: undefined, cnt: 1},
    ],
    remoteClaimer: [
        {targetRoomName: 'E38N52'},
    ],
    defender: [
        {targetRoomName: 'E37N53', cnt: 1},
        {targetRoomName: 'E38N52', cnt: 1},
    ],
    energyLink: [
        {sourceLink: '5850aaca73fff7526c371aab', targetLink: '58506a421ca50dfa68a96d68'},
        {sourceLink: '5857394b75adc0fb4d3df691', targetLink: '58506a421ca50dfa68a96d68'},
    ],
    fixedHarvesters: [
        {x: 29, y: 11, sourceId: '579fa9d00700be0674d2fef8'},
        {x: 5, y: 10, sourceId: '579fa9d00700be0674d2fef6'},
    ],
    lorryRoomtransfer: [
        //{targetRoomName: 'E38N54', cnt: 1},
    ]
};

roomConstants['E37N54'] = {
    displayName: 'Expansion 2',

    targetHarvester: 1,
    targetUpgrader: 0,
    targetBuilder: 3,
    targetRepairer: 0,
    targetRecharger: 0,
    targetMiner: 0,
    targetLorry: 2,
    energySourceLink: undefined,
    remoteBuilder: [
        {targetRoomName: 'E37N55', cnt: 1},
        {targetRoomName: 'E37N53', cnt: 1},
        // {targetRoomName: 'E38N52', cnt: 1},
        {targetRoomName: 'E38N54', cnt: 1},
        // {targetRoomName: 'E39N53', cnt: 1},
    ],
    remoteHarvester: [
        // {targetRoomName: 'E38N52', unloadingLink: '5850aaca73fff7526c371aab', cnt: 3},
        // {targetRoomName: 'E37N53', unloadingLink: undefined, cnt: 2},
        // {targetRoomName: 'E39N53', unloadingLink: '5857394b75adc0fb4d3df691', cnt: 2},
        // {targetRoomName: 'E38N54', unloadingLink: undefined, cnt: 1},
    ],
    remoteClaimer: [
        // {targetRoomName: 'E38N52'},
    ],
    defender: [
        // {targetRoomName: 'E38N52', cnt: 1},
        // {targetRoomName: 'E37N53', cnt: 1},
    ],
    energyLink: [
        // {sourceLink: '5850aaca73fff7526c371aab', targetLink: '58506a421ca50dfa68a96d68'},
        // {sourceLink: '5857394b75adc0fb4d3df691', targetLink: '58506a421ca50dfa68a96d68'},
    ],
    fixedHarvesters: [
        {x: 20, y: 33, sourceId: '579fa9cc0700be0674d2feb8'},
        {x: 6, y: 16, sourceId: '579fa9cc0700be0674d2feb7'},
    ],
    lorryRoomtransfer: [
        //{targetRoomName: 'E38N54', cnt: 1},
    ]
};
