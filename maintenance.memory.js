/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('maintenance.memory');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    cleanup: function () {
        for (let name in Memory.creeps) {
            if (Game.creeps[name] == undefined) {
                // console.log(name);
                Memory.creeps[name] = undefined;
            }
        }
    }
};