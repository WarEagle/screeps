/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * let  mod = require('role.recharger');
 * mod.thing == 'a thing'; // true
 */
let helperNavigation = require('helper.navigation');

module.exports = {
    run: function (creep) {

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working == false && creep.carry.energy >= creep.carryCapacity * 0.75) {
            creep.memory.working = true;
        }

        if (creep.memory.working == true) {
            //distribute energy
            let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_TOWER
                && s.energyCapacity * 0.8 > s.energy
                && s.room == creep.room
            });
            if (structure != undefined) {
                if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                }
            } else if (helperNavigation.unloadingToSpawnOrExtension(creep)) {
            } else if (helperNavigation.unloadingEnergyToStorage(creep)) {
            } else {
                let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (s) => s.structureType == STRUCTURE_LAB
                    && s.energyCapacity * 0.9 > s.energy
                    && s.room == creep.room
                });
                if (structure != undefined) {
                    if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(structure);
                    }
                } else {
                }
            }
        }
        else {
            if (helperNavigation.transferFromContainer(creep)
            ) {

            }
            else {
                helperNavigation.transferFromLink(creep);
            }
        }
    }
}
;