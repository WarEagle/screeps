var roleRecharger = require('role.recharger');
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.transfer');
 * mod.thing == 'a thing'; // true
 */

// Game.spawns.Spawn1.createCreep([MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY], undefined, {role: 'transfer',way: 'E39N55,E39N54,E39N53,E38N53', working: false, newrole: 'worker'});
// Game.spawns.Spawn1.createCreep([MOVE,WORK,CARRY], undefined, {role: 'transfer',way: 'E39N55,E39N54,E39N53,E38N53', newrole: 'upgrader', working: false});
// Game.spawns.Spawn1.createCreep([CLAIM, MOVE, MOVE, MOVE], undefined, {role: 'transfer',way: 'E39N55,E39N54,E39N53,E38N53', targetRoomName:'E38N53', newrole: 'upgrader', working: false});


module.exports = {
    run: function (creep) {

        let way = creep.memory.way.split(',');
        // correct room?
        // console.log(creep.room.name);
        // console.log(way[way.length - 1]);
        if (creep.room.name == way[way.length - 1]) {
            console.log(creep.name + " reached goal!!");
            creep.memory.role = creep.memory.newrole;
            creep.moveTo(15,15);
        }
        else {
            for (let i = 0; i < way.length; i++) {
                if (creep.room.name == way[i]) {
                    let exitDir = Game.map.findExit(creep.room, way[i + 1]);
                    let exit = creep.pos.findClosestByRange(exitDir);
                    creep.moveTo(exit);
                }
            }
        }
    }
};
