/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * let  mod = require('defence.tower');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    repair: function (tower) {
        if (tower.energy < tower.energyCapacity * 0.75) {
            //save energy for defending
            return;
        }
        let structure = tower.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (s) => (
                s.hits < s.hitsMax
                && s.hits / s.hitsMax <= 0.9
                && s.structureType != STRUCTURE_WALL
                && s.structureType != STRUCTURE_RAMPART
            )
        });
        if (structure != undefined) {
            // console.log(structure.hits + "/" + structure.hitsMax)
            tower.repair(structure);
        } else {
            let structure = tower.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) => (
                    s.hits < s.hitsMax
                    && s.hits / s.hitsMax <= 0.0002
                    && s.structureType == STRUCTURE_WALL
                )
            });
            if (structure != undefined) {
                // console.log(structure.hits + "/" + structure.hitsMax)
                tower.repair(structure);
            } else {
                let structure = tower.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (s) => (
                        s.hits < s.hitsMax
                        && s.hits / s.hitsMax <= 0.0002
                        && s.structureType == STRUCTURE_RAMPART
                    )
                });
                if (structure != undefined) {
                    // console.log(structure.hits + "/" + structure.hitsMax)
                    tower.repair(structure);
                }
            }
        }
    },

    defend: function defendRoom(roomName) {

        let hostiles = Game.rooms[roomName].find(FIND_HOSTILE_CREEPS);

        if (hostiles.length > 0) {
            let username = hostiles[0].owner.username;
            Game.notify(`User ${username} spotted in room ${roomName}`);
            let towers = Game.rooms[roomName].find(
                FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
            towers.forEach(tower => tower.attack(hostiles[0]));
            return true;
        }
        return false;
    }
};