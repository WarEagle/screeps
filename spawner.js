/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * let mod = require('spawner');
 * mod.thing == 'a thing'; // true
 */

function getMaxEnergyCapacityAvailable(spawner) {
    return Math.min(1800, spawner.room.energyCapacityAvailable);
}

module.exports = {
    spawn: function (roomName, spawner) {
        // Game.spawns.Spawn2.createCreep([CLAIM, MOVE, MOVE], 'Claim E37N54', {
        //     working: false,
        //     role: 'claimer',
        //     targetRoomName: 'E37N54',
        // });
        // return;

        if (spawner.spawning != null) {
            //don't run if spawner is already spawning
            return;
        }

        // if (Game.time % 4 != 0){
        //     //let's save some cpu
        //     return;
        // }
        //
        let constants = roomConstants[roomName];

        let targetHarvester = roomConstants[roomName].targetHarvester;
        let targetUpgrader = roomConstants[roomName].targetUpgrader;
        let targetBuilder = roomConstants[roomName].targetBuilder;
        let targetRepairer = roomConstants[roomName].targetRepairer;
        let targetRecharger = roomConstants[roomName].targetRecharger;
        let targetMiner = roomConstants[roomName].targetMiner;
        let targetLorry = roomConstants[roomName].targetLorry;
        let targetFixedHarvester = roomConstants[roomName].fixedHarvesters.length;

        let currHarvester = _.sum(Game.creeps, (c) => c.memory.role == 'harvester' && c.room.name == roomName);
        let currUpgrader = _.sum(Game.creeps, (c) => c.memory.role == 'upgrader' && c.room.name == roomName);
        let currBuilder = _.sum(Game.creeps, (c) => c.memory.role == 'builder' && c.room.name == roomName);
        let currRepairer = _.sum(Game.creeps, (c) => c.memory.role == 'repairer' && c.room.name == roomName);
        let currRecharger = _.sum(Game.creeps, (c) => c.memory.role == 'recharger' && c.room.name == roomName);
        let currMiner = _.sum(Game.creeps, (c) => c.memory.role == 'miner' && c.room.name == roomName);
        let currLorry = _.sum(Game.creeps, (c) => c.memory.role == 'lorry' && c.room.name == roomName);
        let currFixedHarvester = _.sum(Game.creeps, (c) => c.memory.role == 'fixedHarvester' && c.room.name == roomName);

        console.log([roomName + ' - ' + constants.displayName,
                "Upgrader: " + currUpgrader + "/" + targetUpgrader,
                "Harvester: " + currHarvester + "/" + targetHarvester,
                "Builder: " + currBuilder + "/" + targetBuilder,
                // "Repairer: " + currRepairer + "/" + targetRepairer,
                // "Recharger: " + currRecharger + "/" + targetRecharger,
                "Miner: " + currMiner + "/" + targetMiner,
                "Lorry: " + currLorry + "/" + targetLorry,
                // "FixedHarvester: " + currFixedHarvester + "/" + targetFixedHarvester
            ]
        );

        if ((currHarvester == 0 && targetHarvester > 0) && (currFixedHarvester == 0 && currLorry == 0)) {
            this.spawnMaxAvail('harvester', spawner);
        } else if (currHarvester < targetHarvester) {
            //console.log(2);
            this.spawnMax('harvester', spawner);
        }
        // else if (this.mustSpawnDefender(roomName, constants, spawner)) {
        // }
        // else if (this.mustSpawnRemoteBuilder(roomName, constants, spawner)) {
        // }
        else if (currUpgrader < targetUpgrader) {
            this.spawnMax('upgrader', spawner);
        }
        else if (currRecharger < targetRecharger) {
            this.spawnMaxCarry('recharger', spawner);
        }
        else if (this.mustSpawnFixedHarvester(roomName, constants, spawner)) {
        }
        else if (this.spawnedRemoteHarvester(roomName, constants, spawner)) {
        }
        else if (this.mustSpawnLorryRoomtransfer(roomName, constants, spawner)) {
        }
        else if (currLorry < targetLorry) {
            this.spawnLorry(spawner);
        }
        else if (currBuilder < targetBuilder) {
            this.spawnMax('builder', spawner);
        }
        else if (currMiner < targetMiner) {
            this.spawnMax('miner', spawner);
        }
        else if (this.mustSpawnClaimer(roomName, constants, spawner)) {
        }
        else if (this.mustSpawnDefender(roomName, constants, spawner)) {
        }
        else if (this.mustSpawnRemoteBuilder(roomName, constants, spawner)) {
            //just to do nothing else
            // else if (currRepairer < targetRepairer) {
            //     this.spawnMax('repairer', spawner);
            // }
        } else {
            console.log(roomName + " complete");
        }
//        else if (currTransfer < 5) {
//            Game.spawns.Spawn1.createCreep([MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY], undefined, {role: 'transfer',way: 'E39N55,E39N54,E39N53,E38N53', working: false, newrole: 'builder'});
//        }
    },

    spawnMax: function (givenRole, spawner) {
        let nrComponents = Math.floor(getMaxEnergyCapacityAvailable(spawner) / 200);
        // console.log("spawner:"+spawner.pos.roomName);
        // console.log("energyCapacityAvailable:"+Game.spawns.Spawn1.room.energyCapacityAvailable);
        // console.log("nrComponents:"+nrComponents);

        let components = [];
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
        }
        spawner.createCreep(components, undefined, {
            working: false,
            role: givenRole,
        });
    },

    spawnMaxCarry: function (givenRole, spawner) {
        let nrComponents = Math.floor(getMaxEnergyCapacityAvailable(spawner) / 150);
        let components = [];
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        spawner.createCreep(components, undefined, {
            working: false,
            role: givenRole,
        });

    },

    spawnMaxAvail: function (givenRole, spawner) {
        let nrComponents = Math.floor(spawner.room.energyAvailable / 200);
        // console.log("energyCapacityAvailable:"+Game.spawns.Spawn1.room.energyCapacityAvailable);
        // console.log("nrComponents:"+nrComponents);
        let components = [];
        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
        }
        spawner.createCreep(components, undefined, {
            working: false,
            role: givenRole,
        });
    },

    spawnLdHarvester: function (targetRoomName, spawner, baseRoomName, roomUnloadingLink) {
        let nrComponents = Math.floor(getMaxEnergyCapacityAvailable(spawner) / 300);
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents * 2.3; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
        }
        let rc = spawner.createCreep(components, undefined, {
            working: false,
            role: 'ldHarvester',
            targetRoomName: targetRoomName,
            baseRoomName: baseRoomName,
            roomUnloadingLink: roomUnloadingLink,
        });
        return _.isString(rc);
    },

    spawnRemoteBuilder: function (targetRoomName, spawner, baseRoomName) {
        let nrComponents = Math.floor(getMaxEnergyCapacityAvailable(spawner) / 250);
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents * 2; i++) {
            components.push(CARRY);
        }
        for (let i = 0; i < nrComponents; i++) {
            components.push(WORK);
        }
        spawner.createCreep(components, undefined, {
            working: false,
            role: 'remoteBuilder',
            targetRoomName: targetRoomName,
            baseRoomName: baseRoomName,
        });
    },

    spawnLorryRoomtransfer: function (targetRoomName, spawner, baseRoomName) {
        let nrComponents = Math.floor(getMaxEnergyCapacityAvailable(spawner) / 150);
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents * 2; i++) {
            components.push(CARRY);
        }
        spawner.createCreep(components, undefined, {
            working: false,
            role: 'lorryRoomtransfer',
            targetRoomName: targetRoomName,
            baseRoomName: baseRoomName,
        });
    },

    spawnFixedHarvester: function (spawner, x, y, sourceId) {
        let components = [];

        components.push(WORK);
        components.push(WORK);
        components.push(WORK);
        components.push(WORK);
        components.push(WORK);
        // components.push(WORK);
        components.push(MOVE);
        components.push(MOVE);

        let rc = spawner.createCreep(components, undefined, {
            working: false,
            role: 'fixedHarvester',
            x: x,
            y: y,
            sourceId: sourceId,
        });
        return _.isString(rc);
    },

    spawnLorry: function (spawner) {
        let nrComponents = Math.floor(getMaxEnergyCapacityAvailable(spawner) / 175);
        let components = [];

        for (let i = 0; i < nrComponents; i++) {
            components.push(MOVE);
        }
        for (let i = 0; i < nrComponents * 1.5; i++) {
            components.push(CARRY);
        }
        let rc = spawner.createCreep(components, undefined, {
            working: false,
            role: 'lorry',
        });
        return _.isString(rc);
    },

    spawnClaimer: function (targetRoomName, baseRoomName, spawner) {
        let components = [];

        // components.push(TOUGH);
        // components.push(TOUGH);
        // components.push(TOUGH);
        // components.push(MOVE);
        // components.push(MOVE);
        // components.push(MOVE);
        // components.push(MOVE);
        // components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(CLAIM);
        components.push(CLAIM);

        spawner.createCreep(components, undefined, {
            working: false,
            role: 'claimer',
            targetRoomName: targetRoomName,
            baseRoomName: baseRoomName,
        });
    },

    spawnDefender: function (targetRoomName, baseRoomName, spawner) {
        let components = [];

        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);
        components.push(TOUGH);

        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);
        components.push(MOVE);

        components.push(ATTACK);
        components.push(ATTACK);
        components.push(RANGED_ATTACK);
        components.push(RANGED_ATTACK);

        spawner.createCreep(components, undefined, {
            working: false,
            role: 'defender',
            targetRoomName: targetRoomName,
            baseRoomName: baseRoomName,
        });
    },

    spawnedRemoteHarvester: function (roomName, constants, spawner) {
        let hasSpawned = false;

        for (let remoteRoom of constants.remoteHarvester) {
            let currLongDistanceHarvester = _.sum(Game.creeps, (c) => (
                c.memory.role == 'ldHarvester'
                && c.memory.baseRoomName == roomName
                && c.memory.targetRoomName == remoteRoom.targetRoomName
            ));
            console.log('   RH ' + roomName + '->' + remoteRoom.targetRoomName + ': ' + currLongDistanceHarvester + '/' + remoteRoom.cnt);
            if (currLongDistanceHarvester < remoteRoom.cnt) {
                //spawn new one
                hasSpawned = this.spawnLdHarvester(remoteRoom.targetRoomName, spawner, roomName, remoteRoom.unloadingLink);
                break;
            }
        }
        return hasSpawned;
    },

    mustSpawnRemoteBuilder: function (roomName, constants, spawner) {
        let hasSpawned = false;

        for (let remoteRoom of constants.remoteBuilder) {
            let currRemoteBuilder = _.sum(Game.creeps, (c) => (
                c.memory.role == 'remoteBuilder'
                // && c.memory.baseRoomName == roomName
                && c.memory.targetRoomName == remoteRoom.targetRoomName
            ));
            console.log('   RB ' + roomName + '->' + remoteRoom.targetRoomName + ': ' + currRemoteBuilder + '/' + remoteRoom.cnt);
            if (currRemoteBuilder < remoteRoom.cnt) {
                //spawn new one
                this.spawnRemoteBuilder(remoteRoom.targetRoomName, spawner, roomName);
                hasSpawned = true;
                break;
            }
        }
        return hasSpawned;
    },

    mustSpawnFixedHarvester: function (roomName, constants, spawner) {
        if (constants.fixedHarvesters.length == 0) {
            return false;
        }
        let hasSpawned = false;

        let currCount = _.sum(Game.creeps, (c) => (
            c.memory.role == 'fixedHarvester'
            && c.room.name == roomName
        ));
        // console.log("currCount "+currCount);
        console.log('   FH ' + roomName + ': ' + currCount + '/' + constants.fixedHarvesters.length);
        if (currCount >= constants.fixedHarvesters.length) {
            // nothing to do
            return false;
        }
        // console.log(1);

        //spawn new one
        for (let harvester of constants.fixedHarvesters) {
            let currCount = _.sum(Game.creeps, (c) => (
                c.memory.role == 'fixedHarvester'
                && c.room.name == roomName
                && c.memory.x == harvester.x
                && c.memory.y == harvester.y
            ));
            // console.log("currCount2 "+currCount);
            if (currCount < 1) {
                // console.log(2);
                // this specific does not exist, so create it
                hasSpawned = this.spawnFixedHarvester(spawner, harvester.x, harvester.y, harvester.sourceId);
                // console.log("4" + " " + hasSpawned);

                break;
            }
            // console.log(3);

        }
        return hasSpawned;
    },

    mustSpawnLorryRoomtransfer: function (roomName, constants, spawner) {
        let hasSpawned = false;

        for (let remoteRoom of constants.lorryRoomtransfer) {
            let current = _.sum(Game.creeps, (c) => (
                c.memory.role == 'lorryRoomtransfer'
                // && c.memory.baseRoomName == roomName
                && c.memory.targetRoomName == remoteRoom.targetRoomName
            ));
            console.log('   LR ' + roomName + '->' + remoteRoom.targetRoomName + ': ' + current + '/' + remoteRoom.cnt);
            if (current < remoteRoom.cnt) {
                //spawn new one
                this.spawnLorryRoomtransfer(remoteRoom.targetRoomName, spawner, roomName);
                hasSpawned = true;
                break;
            }
        }
        return hasSpawned;
    },

    mustSpawnClaimer: function (roomName, constants, spawner) {
        let hasSpawned = false;

        for (let remoteRoom of constants.remoteClaimer) {
            let currClaimer = _.sum(Game.creeps, (c) => (
                c.memory.role == 'claimer'
                // && c.memory.baseRoomName == roomName
                && c.memory.targetRoomName == remoteRoom.targetRoomName
            ));
            console.log('   CL ' + roomName + '->' + remoteRoom.targetRoomName + ': ' + currClaimer + '/' + 1);
            if (currClaimer < 1) {
                //spawn new one
                this.spawnClaimer(remoteRoom.targetRoomName, roomName, spawner);
                hasSpawned = true;
                break;
            }
        }
        return hasSpawned;
    }
    ,

    mustSpawnDefender: function (roomName, constants, spawner) {
        let hasSpawned = false;

        for (let remoteRoom of constants.defender) {
            let currentCreeps = _.sum(Game.creeps, (c) => (
                c.memory.role == 'defender'
                // && c.memory.baseRoomName == roomName
                && c.memory.targetRoomName == remoteRoom.targetRoomName
            ));
            console.log('   DF ' + roomName + '->' + remoteRoom.targetRoomName + ': ' + currentCreeps + '/' + 1);
            if (currentCreeps < remoteRoom.cnt) {
                //spawn new one
                this.spawnDefender(remoteRoom.targetRoomName, roomName, spawner);
                hasSpawned = true;
                break;
            }
        }
        return hasSpawned;
    }


}
;