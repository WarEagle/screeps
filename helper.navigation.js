/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('helper.navigation');
 * mod.thing == 'a thing'; // true
 */


module.exports = {
    moveToRoom: function (creep, roomName) {
        // correct room?
        if (creep.room.name == roomName) {
            return true;
        }
        else {
            let route = Game.map.findRoute(creep.pos.roomName, roomName);
            let exit = Game.map.findExit(creep.room, route[0].room);
            let direction = creep.pos.findClosestByRange(exit);
            //console.log(JSON.stringify(direction));
            creep.moveTo(direction);
            return false;
        }
    },

    canTakeEnergyFromReceiverLink: function (creep) {
        let link = roomConstants[creep.room.name]
        if (link != undefined && link.energySourceLink != undefined) {
            let loadingLink = Game.getObjectById(link.energySourceLink);
            if (loadingLink != undefined && loadingLink.energy > 0) {
                //console.log(creep.withdraw(loadingLink, RESOURCE_ENERGY));
                if (creep.withdraw(loadingLink, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(loadingLink);
                }
            } else {
                let source = creep.pos.findClosestByPath(FIND_DROPPED_ENERGY, {
                        filter: (x) => (x.room == creep.room)
                    })
                    ;
                if (source) {
                    if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(source);
                    }
                }
            }
        }
    },

    unloadingToSpawnOrExtension: function (creep) {
        let structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
            filter: (s) => (
                (s.structureType == STRUCTURE_SPAWN || s.structureType == STRUCTURE_EXTENSION)
                && s.room == creep.room
                && s.energy < s.energyCapacity
            )
        });
        if (structure != undefined) {
            if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        }
        return false;
    },

    unloadingEnergyToStorage: function (creep) {
        let structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
            filter: (s) => (
                (s.structureType == STRUCTURE_STORAGE
                && s.room == creep.room)
                // && s.energy < s.energyCapacity
            )
        });
        if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            creep.moveTo(structure);
            return true;
        }
        return false;
    },

    unloadingMineralsToStorage: function (creep) {
        let structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
            filter: (s) => (
                (s.structureType == STRUCTURE_STORAGE
                && s.room == creep.room)
                // && s.energy < s.energyCapacity
            )
        });
        for (let ressource in RESOURCES_ALL) {
            //console.log(JSON.stringify(creep.carry));
            if (creep.carry[RESOURCES_ALL[ressource]] != undefined && creep.carry[RESOURCES_ALL[ressource]] > 0) {
                //console.log(creep.carry[RESOURCES_ALL[ressource]]);
                if (creep.transfer(structure, RESOURCES_ALL[ressource]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                    return true;
                }
                break;
            }
        }
        return false;
    },

    transferFromContainer: function (creep) {
        let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (s) => s.structureType == STRUCTURE_CONTAINER
            && s.store[RESOURCE_ENERGY] >= creep.carryCapacity * 0.9
            && s.room == creep.room
        });

        if (structure != undefined) {
            if (creep.withdraw(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        } else {
            return false;
        }
    },

    transferFromLink: function (creep) {
        let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (s) => s.structureType == STRUCTURE_LINK
            && s.energy > 200
            && s.room == creep.room
        });
        if (structure != undefined) {
            if (creep.withdraw(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        } else {
            return false;
        }
    },

    takeEnergyFromStorage: function (creep) {
        //take from storage
        let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
            filter: (s) => s.structureType == STRUCTURE_STORAGE
            && s.store[RESOURCE_ENERGY] >= creep.carryCapacity
            && s.room == creep.room
        });
        if (structure != undefined) {
            if (structure.transfer(creep, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(structure);
            }
            return true;
        } else {
            return false;
        }
    },

    takeEnergyFromEnemyBuilding: function (creep) {
        for (let mineral in RESOURCES_ALL) {
            // console.log(mineral);
            let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) =>
                (
                    s.structureType == STRUCTURE_LAB
                    || s.structureType == STRUCTURE_EXTENSION
                )
                // && s.store[RESOURCES_ALL[mineral]] >= 100
                && s.energy > 0
                && s.room == creep.room
                && s.owner != creep.owner
            });
            if (structure != undefined) {
                if (structure.transfer(creep, RESOURCES_ALL[mineral]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                }
                // console.log(true);
                return true;
            }
        }
        // console.log(false);
        return false;
    },

    takeMineralsFromStorage: function (creep) {
        //take from storage
        for (let mineral in RESOURCES_ALL) {
            // console.log(mineral);
            let structure = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_STORAGE
                && s.store[RESOURCES_ALL[mineral]] >= 100
                && s.room == creep.room
            });
            if (structure != undefined) {
                if (structure.transfer(creep, RESOURCES_ALL[mineral]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structure);
                }
                // console.log(true);
                return true;
            }
        }
        // console.log(false);
        return false;
    },


}
;