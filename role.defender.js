var roleBuilder = require('role.builder');
var helperNavigation = require('helper.navigation');
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.defender');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function (creep) {

        // correct room?
        if (helperNavigation.moveToRoom(creep, creep.memory.targetRoomName)) {

            //first try ranged
            let targets = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 3);
            if(targets.length > 0) {
                creep.rangedAttack(targets[0]);
            }

            //then attack melee
            targets = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 1);
            if(targets.length > 0) {
                if(creep.attack(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            }

            //and now move closer
            let closedTarget = creep.pos.findClosestByPath(FIND_HOSTILE_CREEPS);
            creep.moveTo(closedTarget);
        }
    }

};