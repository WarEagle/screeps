let roleHarvester = require('role.harvester');
let roleUpgrader = require('role.upgrader');
let roleBuilder = require('role.builder');
let roleRepairer = require('role.repairer');
let roleRecharger = require('role.recharger');
let roleLdHarvster = require('role.lgHarvester');
let roleClaimer = require('role.claimer');
let roleTransfer = require('role.transfer');
let roleRemoteBuilder = require('role.remoteBuilder');
let roleDefender = require('role.defender');
let roleMiner = require('role.miner');
let roleFixedHarvester = require('role.fixedHarvester');
let roleLorry = require('role.lorry');
let roleLorryRoomtransfer = require('role.lorryRoomtransfer');

let room = require('room');

var helperConstants = require('helper.constants');


module.exports.loop = function () {
    // Makes sure memory is clear of dead creeps
    for (let name in Memory.creeps) {
        if (!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

    for (let spawner in Game.spawns){
        let spawn = Game.spawns[spawner];
        //console.log(spawn.name);
        room.manage(spawn.pos.roomName, spawn);
    }

    // Letting the creeps do their jobs
    for (let name in Game.creeps) {
        let creep = Game.creeps[name];
        if (creep.spawning == true){
            continue;
        }

        if (creep.fatigue > 0){
            continue;
        }

        if (creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        else if (creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        else if (creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        else if (creep.memory.role == 'repairer') {
            roleRepairer.run(creep);
        }
        else if (creep.memory.role == 'recharger') {
            roleRecharger.run(creep);
        }
        else if (creep.memory.role == 'ldHarvester') {
            roleLdHarvster.run(creep);
        }
        else if (creep.memory.role == 'claimer') {
            roleClaimer.run(creep);
        }
        else if (creep.memory.role == 'transfer') {
            roleTransfer.run(creep);
        }
        else if (creep.memory.role == 'remoteBuilder') {
            roleRemoteBuilder.run(creep);
        }
        else if (creep.memory.role == 'defender') {
            roleDefender.run(creep);
        }
        else if (creep.memory.role == 'miner') {
            roleMiner.run(creep);
        }
        else if (creep.memory.role == 'fixedHarvester') {
            roleFixedHarvester.run(creep);
        }
        else if (creep.memory.role == 'lorry') {
            roleLorry.run(creep);
        }
        else if (creep.memory.role == 'lorryRoomtransfer') {
            roleLorryRoomtransfer.run(creep);
        }else{
            console.log("UNKNOWN ROLE! "+creep.name)
        }
    }

};