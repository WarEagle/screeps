var roleRecharger = require('role.recharger');
/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.lgHarvester');
 * mod.thing == 'a thing'; // true
 */

module.exports = {
    run: function (creep, removeMe) {
        roomUnloadingLink = creep.memory.roomUnloadingLink;

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        }

        if (creep.memory.working == false && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.ticksToLive < 200) {
            creep.memory.working = true;
        }

		if (creep.memory.working == true) {
			// correct room?
			if(creep.room.name != creep.memory.baseRoomName) {
				let exitDir = Game.map.findExit(creep.room, creep.memory.baseRoomName);
				let exit = creep.pos.findClosestByRange(exitDir);
				creep.moveTo(exit);
			}
			else {
                let unloadingLink = Game.getObjectById(roomUnloadingLink);
                // console.log(roomUnloadingLink);
			    if (unloadingLink != undefined && unloadingLink.energy < unloadingLink.energyCapacity) {
					if (creep.transfer(unloadingLink, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
						creep.moveTo(unloadingLink);
					}
			    }else{
    				let structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
    					filter: (s) => (
    						(s.structureType == STRUCTURE_SPAWN || s.structureType == STRUCTURE_EXTENSION)
    						&& s.energy < s.energyCapacity
    						&& s.room == creep.room
    					)
    				});
    				if (structure != undefined) {
    					if (creep.transfer(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
    						creep.moveTo(structure);
    					}
    				// } else {
    				// 	if (creep.transfer(Game.spawns.Spawn1, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
    				// 		creep.moveTo(Game.spawns.Spawn1);
    				// 	}
    				}else {
                        roleRecharger.run(creep);
                    }
			    }
			}
		} else {
			// correct room?
			if(creep.room.name != creep.memory.targetRoomName) {
				let exitDir = Game.map.findExit(creep.room, creep.memory.targetRoomName);
                let exit = creep.pos.findClosestByRange(exitDir);
				creep.moveTo(exit);
			}
			else {
				// go to some place in another room
				let source = creep.pos.findClosestByPath(FIND_SOURCES, {
						filter: (x) => x.energy > 0
                        && x.room == creep.room
					})
					;
				if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    //console.log(creep.name + " "+creep.moveTo(source));
					if (creep.moveTo(source) == ERR_NO_PATH){
                        creep.moveTo(2,22);
                    }
					//console.log("    "+creep.room.name + " " + creep.name + " " + source);
				}
			}
		}
    }
};