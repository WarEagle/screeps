/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.harvester');
 * mod.thing == 'a thing'; // true
 */

var unloadingToStorage = function (creep) {
    let structure = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
        filter: (s) => (
            (s.structureType == STRUCTURE_STORAGE
            && s.room == creep.room)
        )
    });
    //loop on resource_all?
    if (creep.transfer(structure, RESOURCE_HYDROGEN) == ERR_NOT_IN_RANGE) {
        creep.moveTo(structure);
        return true;
    }
    return false;
};

module.exports = {
    run: function (creep) {

        if (creep.memory.working == true && (creep.carry.H == undefined || creep.carry.H == 0)) {
            creep.memory.working = false;
        }

        //loop on resource_all?
        if (creep.memory.working == false && creep.carry.H >= creep.carryCapacity) {
            creep.memory.working = true;
        }

        //come back when end is near
        if (creep.ticksToLive < 200){
            creep.memory.working = true;
        }


        if (creep.memory.working == true) {
            unloadingToStorage(creep);
        } else {
            //collect minerals
            let source = creep.pos.findClosestByPath(FIND_MINERALS, {
                    filter: (x) => (
                    x.room == creep.room)
                })
                ;
            if (source) {
                //console.log(creep.harvest(source));
                if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(source);
                }
            }else{
                console.log(creep.name + " no extractor found.");
            }
        }
    }
};